# HuskyLang v1.0b Changelog

- Updated Syntax to use New<Type>(name, value) for all types
- Refactored Parser 
- Moved all packages under a main package named HuskyLang
