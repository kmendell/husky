<div id="header" align="center">
  <h1>HuskyLang built in GO</h1>
  <img src="https://gitlab.com/kmendell/husky/-/raw/main/kisspng-gray-wolf-logo-photography-wolf-logo-5ae014b91cc3e1.5206582615246348091178.png?inline=false" />
<div>

<div id="body" align="left">

Example husky File

```go

project main

// Comments Can only be used on seperate lines like this one

// This is a example of defining a new string
NewString(stringname, "this is my new string")
print(stringname)

// This is updating the previous string we defined to a new value
UpdateString(stringname, "this is the updated string")
print(stringname)

// Same can be done for other types as well
NewNumber(mynumber, 1)
print(mynumber)

UpdateNumber(mynumber, 100)
print(mynumber)

NewBool(mybool, true)
print(mybool)

```

check this out for new parser 

https://github.com/alecthomas/participle/blob/master/_examples/thrift/main.go
</div>

