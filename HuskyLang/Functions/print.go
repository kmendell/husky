package Functions

import (
	"ofkm.us/husky/HuskyLang/Types/HuskyProject"
	"strconv"
	"strings"
)

func PrintParse(Line string) {
	part1 := strings.Split(Line, "(")
	part2 := strings.Split(part1[1], ")")
	for _, x := range HuskyProject.CompiledHuskyProject.HuskyVariables {
		if x.Name == part2[0] {
			switch x.Type {
			case "string":
				println(x.Value)
			case "number":
				println(strconv.Atoi(x.Value))
			case "bool":
				boolval := false
				if strings.ToLower(x.Value) == "true" || strings.ToLower(x.Value) == "yes" || strings.ToLower(x.Value) == "1" {
					boolval = true
				} else {
					boolval = false
				}
				println(boolval)
			}
		} else {
			// println("Print value inside quotes")
		}
	}
	// Println(part2[0])
}

func Println(varname string) {

	// for _, x := range HuskyProject.CompiledHuskyProject.HuskyStrings {
	// 	if x.Name == varname {
	// 		println(x.Value)
	// 	}
	// }

	// for _, x := range HuskyProject.CompiledHuskyProject.HuskyInts {
	// 	if x.Name == varname {
	// 		println(x.Value)
	// 	}
	// }

	// for _, x := range HuskyProject.CompiledHuskyProject.HuskyBools {
	// 	if x.Name == varname {
	// 		println(x.Value)
	// 	}
	// }
}
