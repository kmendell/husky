package Parser

import (
	"fmt"

	wildcard "github.com/IGLOU-EU/go-wildcard"
)

func HuskyParsev2(Line string) {

	pattern := "NewString(*, *)"
	result := wildcard.MatchSimple(pattern, Line)
	if result {
		fmt.Println(pattern, result)
	}

	pattern = "UpdateString(*, *)"
	result = wildcard.MatchSimple(pattern, Line)
	if result {
		fmt.Println(pattern, result)
	}

	pattern = "NewNumber(*, *)"
	result = wildcard.MatchSimple(pattern, Line)
	if result {
		fmt.Println(pattern, result)
	}

	pattern = "UpdateNumber(*, *)"
	result = wildcard.MatchSimple(pattern, Line)
	if result {
		fmt.Println(pattern, result)
	}

	pattern = "NewBool(*, *)"
	result = wildcard.MatchSimple(pattern, Line)
	if result {
		fmt.Println(pattern, result)
	}

	pattern = "UpdateBool(*, *)"
	result = wildcard.MatchSimple(pattern, Line)
	if result {
		fmt.Println(pattern, result)
	}

	pattern = "print(*)"
	result = wildcard.MatchSimple(pattern, Line)
	if result {
		fmt.Println(pattern, result)
	}
}
